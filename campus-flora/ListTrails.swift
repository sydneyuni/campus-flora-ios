//
//  ListTrails.swift
//  CampusFlora
//
//  Created by ERU on 17/11/2015.
//  Copyright © 2015 Campus Flora. All rights reserved.
//

import UIKit

class ListTrails: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var trails: JSON!
    var index: [Index]!
    
    @IBOutlet weak var Table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Table.registerClass(UITableViewCell.self, forCellReuseIdentifier: trailIdentifier)
        Table.delegate = self
        Table.dataSource = self
        
        trails = JSONParser.sharedInstance.trails
        index = JSONParser.sharedInstance.indexT
        
        // remove extra sections
        Table.tableFooterView = UIView()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return index.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: trailIdentifier)
        cell.textLabel?.text = trails[index[indexPath.row].id]["name"].stringValue
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let speciesSet = JSONParser.sharedInstance.findSpeciesFromTrail(trails[index[indexPath.row].id]["id"].stringValue)
        //print("list trails")
        //print(speciesSet)
        let nav = self.navigationController as! CFNavigation
        nav.nextScreen(.map, current: .trails, sp: speciesSet)
    }
    
    func goHome() {
        let nav = self.navigationController as! CFNavigation
        nav.nextScreen(.map)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}