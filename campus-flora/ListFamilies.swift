//
//  ListFamilies.swift
//  CampusFlora
//
//  Created by ERU on 17/11/2015.
//  Copyright © 2015 Campus Flora. All rights reserved.
//

import UIKit

class ListFamilies: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var families: JSON!
    var index: [Index]!
    var selectedFamilies: NSMutableSet!
    var changed = false
    
    @IBOutlet weak var Table: UITableView!
    @IBOutlet weak var SelectionButton: UIButton!
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //print("THIS VIEW SHALL DISAPPEARRRRR")
        let nav = self.navigationController as! CFNavigation
        nav.setSelectedFamilies(selectedFamilies)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Table.registerClass(UITableViewCell.self, forCellReuseIdentifier: familyIdentifier)
        Table.delegate = self
        Table.dataSource = self
        
        families = JSONParser.sharedInstance.families
        index = JSONParser.sharedInstance.indexF
        
        SelectionButton.addTarget(self, action: "familySelectionChanged", forControlEvents: .TouchUpInside)
        //print("\(families)")
        //selectionChanged()
    }

    // selection button functions
    func familySelectionChanged() {
        if (SelectionButton.titleLabel?.text == "Select All") {
            //print("check all the things!")
            selectedFamilies = []
            for i in index {
                selectedFamilies.addObject(i.id)
            }
            //SelectionButton.titleLabel?.text = "Clear All"
            changed = true
        }
        else if (SelectionButton.titleLabel?.text == "Clear All") {
            //print("clear all the things!")
            selectedFamilies = []
            //SelectionButton.titleLabel?.text = "Check All"
            changed = true
        }
        else {
            
        }
        
        // refresh table
        Table.reloadData()
        selectionChanged()
    }
    
    func selectionChanged() {
        print("family selected count: \(selectedFamilies.count)")
        
        if (selectedFamilies.count == 0) {
            SelectionButton.setTitle("Select All", forState: .Normal)
        }
        else {
            SelectionButton.setTitle("Clear All", forState: .Normal)
        }
    }
    
    func preventSelection() {
        let alert = UIAlertController(title: "At most 15 families can be mapped at a time", message: "", preferredStyle: .Alert)
        self.presentViewController(alert, animated: true) {
            alert.view.superview?.userInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "alertClose"))
        }
        
        // dismiss after 3 seconds
        let delay = 3.0 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue(), {
            alert.dismissViewControllerAnimated(true, completion: nil)
        })
    }
    
    func alertClose() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // table
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return index.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: familyIdentifier)
        //print("blarhjsghjldfs: \(index[indexPath.row])")
        cell.textLabel?.text = families[index[indexPath.row].id]["name"].string!

        if (selectedFamilies.containsObject(index[indexPath.row].id)) {
            cell.accessoryType = .Checkmark
        }
        else {
            cell.accessoryType = .None
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        // if more than 10 selected then prevent more selection
        
        changed = true
        
        if (cell?.accessoryType == .Checkmark) {
            cell?.accessoryType = .None
            selectedFamilies.removeObject(index[indexPath.row].id)
            //print("made a remove call \(selectedFamilies)")
        }
        // prevent more than 20 selections at a time
        else if (selectedFamilies.count >= 15) {
            preventSelection()
        }
        else {
            cell?.accessoryType = .Checkmark
            selectedFamilies.addObject(index[indexPath.row].id)
            //print("made an add call \(selectedFamilies)")
        }
        
        selectionChanged()
    }
    
    // home actions
    func mapFamilies() {
        //print("go home you're drunk!")
        if (selectedFamilies.count >= 15) {
            preventSelection()
        }
        else {
            let speciesSet = JSONParser.sharedInstance.findSpeciesFromFamilies(selectedFamilies)
            let nav = self.navigationController as! CFNavigation
            // need to check how much of this is necessary BLAHOFHODSUFHOUFHDOU
            nav.setSelectedFamilies(selectedFamilies)
            nav.nextScreen(.map, current: .families, sp:speciesSet)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}