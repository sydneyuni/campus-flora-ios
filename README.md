# Campus Flora #

Campus Flora maps the locations of more than 1000 individual plants from over 40 families on campus grounds and provides a botanical description of each plant and information on its distribution.

### How do I get set up? ###

From the project directory, install CocoaPods:

```
$ gem install cocoapods
$ pod install
```

Then open CampusFlora.xcworkspace (*not* .xcodeproj) in Xcode.


### Who do I talk to? ###

Business owner:  Dr. Rosanne Quinnell (Biological Sciences eResearch Unit)

Technical support:  <soles.eresources@sydney.edu.au>